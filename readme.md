# Simple Image Processor

## Description
This is a simple java console program which can convert color images to grayscaled ones.

## Prerequisites
You need JDK8 installed

### Build
Build the project from project root directory :
>./gradlew clean build

### Launch
Run program from the same root folder:
>java -jar ./build/libs/test-image-processor-1.0.jar -s file:///\`pwd\`/src/test/resources/images/test.png -d ./processed -w 300 -h 300

or read usage:
>java -jar ./build/libs/test-image-processor-1.0.jar -help 