package com.opensoft.test;

import java.io.PrintWriter;

public class ConsoleLogger implements Logger {

    private final PrintWriter infoWriter = new PrintWriter(System.out);
    private final PrintWriter errWriter = new PrintWriter(System.err);

    @Override
    public void info(String msg) {
        infoWriter.write(msg + "\n");
        infoWriter.flush();
    }

    @Override
    public void error(String msg) {
        errWriter.write(msg + "\n");
        errWriter.flush();
    }
}
