package com.opensoft.test;



import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;

public class GrayFilter implements ImageProcessor {

    protected final ImageFilter imageFilter = new javax.swing.GrayFilter(true, 50);

    @Override
    public BufferedImage process(BufferedImage colorImage) {

        ImageProducer producer = new FilteredImageSource(colorImage.getSource(), imageFilter);
        Image result = Toolkit.getDefaultToolkit().createImage(producer);
        return toBufferedImage(result);
    }

}
