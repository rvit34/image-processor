package com.opensoft.test;


import java.awt.image.BufferedImage;

public class GrayScale implements ImageProcessor {

    private final ImageProcessor grayFilter;
    private final ImageProcessor scaler;

    GrayScale(ImageProcessor grayFilter, ImageProcessor scaler) {
        this.grayFilter = grayFilter;
        this.scaler = scaler;
    }

    @Override
    public BufferedImage process(BufferedImage source) {
        return scaler.process(grayFilter.process(source));
    }
}
