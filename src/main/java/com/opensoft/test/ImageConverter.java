package com.opensoft.test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

class ImageConverter {

    private final ImageProcessor grayScale;
    private final Logger logger;

    ImageConverter(ImageProcessor grayScale, Logger logger) {
        this.grayScale = grayScale;
        this.logger = logger;
    }

    void convert(URL src, File dest) {

        String fileName = dest.getName();
        String imageFormat = fileName.substring(fileName.lastIndexOf('.') + 1);

        logger.info(String.format("start converting file from resource %s", src.toString()));
        BufferedImage srcImage;
        try {
            srcImage = ImageIO.read(src);
            if (srcImage == null) {
                logger.error(String.format("unable to read resource %s", src.toString()));
                return;
            }
        } catch (IOException e) {
            logger.error(String.format("unable to read image. Details: %s", e.getMessage()));
            return;
        }

        long start = System.currentTimeMillis();
        BufferedImage convertedImage;
        try {
            logger.info("converting image...");
            convertedImage = grayScale.process(srcImage);
            long end = System.currentTimeMillis() - start;
            logger.info(String.format("image was converted successfully for %d millisec", end));
        } catch (Exception e) {
            logger.error(String.format("unable to process image. Details: %s", e.getMessage()));
            return;
        }

        logger.info(String.format("storing results to %s...", dest.getAbsolutePath()));
        try {
            ImageIO.write(convertedImage, imageFormat, dest);
            logger.info("done");
        } catch (IOException e) {
            logger.error(String.format("unable to store result. Details: %s", e.getMessage()));
        }
    }
}
