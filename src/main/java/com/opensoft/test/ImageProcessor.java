package com.opensoft.test;

import java.awt.*;
import java.awt.image.BufferedImage;

public interface ImageProcessor {

    BufferedImage process(BufferedImage source);

    default BufferedImage toBufferedImage(Image img) {
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }

        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        final Graphics2D bGr = bimage.createGraphics();

        try {
            bGr.drawImage(img, 0, 0, null);
        } finally {
            bGr.dispose();
        }

        return bimage;
    }
}
