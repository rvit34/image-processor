package com.opensoft.test;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Paths;

public class Launcher {

    private static final Logger logger = new ConsoleLogger();

    public static void main(String[] args) throws MalformedURLException {
        checkArgs(args);
        createConverter(args).convert(
                new URL(getArgValue("-s", args)),
                getDestFile(args)
        );
    }

    private static ImageConverter createConverter(String[] args) {

        int w = Integer.parseInt(getArgValue("-w", args));
        int h = Integer.parseInt(getArgValue("-h", args));
        String alg = getArgValue("-a", args);
        int a = alg.isEmpty() ? 0: Integer.parseInt(alg);

        ImageProcessor[] algs = new ImageProcessor[2];
        algs[0] = new GrayFilter();
        algs[1] = new MultiThreadedGrayFilter(
                Runtime.getRuntime().availableProcessors(),
                logger
        );

        return new ImageConverter(
                new GrayScale(
                        algs[a],
                        new Scaler(w, h)
                ),
                logger
        );
    }

    private static final String USAGE = "Usage:\n" +
            "java -jar [program.jar] -s [IMAGE_URL] -d [DEST_DIR_PATH] -a [IMAGE_PROCESSING_ALG] -w [IMAGE_WIDTH] -h [IMAGE_HEIGHT]\n" +
            "-s: unified resource location where the program can load an image from (use file:///path for local resources)\n" +
            "-d: (optional) destination folder path where the program will store the image to (Default: current directory)\n" +
            "-a: (optional) algorithm is used to process image:\n" +
            "    0 - sequential grayscale (default)\n" +
            "    1 - multithreaded grayscale\n" +
            "-w: width of converted image\n" +
            "-h: height of converted image\n" +
            "Additional Notes:\n" +
            "The program was tested on next image formates: png, bmp and jpg\n" +
            "Correct work on other formats is not guaranteed.";


    private static void checkArgs(String[] args) {

        if (args != null && (hasArg("-help", args))) {
            logger.info(USAGE);
            System.exit(0);
        }

        if (args == null || args.length < 6) {
            logger.error("You have not specified all or some program arguments." + USAGE);
            System.exit(1);
        }

        checkAlgoArg(args);
        checkImageSrcArg(args);
        checkImageResolutionArgs(args);
    }

    private static void checkAlgoArg(String[] args) {
        String alg = getArgValue("-a", args);
        if (!alg.isEmpty()) {
            try {
                int a = Integer.parseInt(alg);
                if (a < 0 || a > 2) {
                    logger.error(String.format("%d is not corresponding to any algo", a));
                    System.exit(1);
                }

            } catch (NumberFormatException e) {
                logger.error("specify a number according to an algorithm");
                System.exit(1);
            }
        }
    }

    private static void checkImageSrcArg(String[] args) {
        String url = getArgValue("-s", args);
        if (url.isEmpty()) {
            logger.error("image url is not specified." + USAGE);
            System.exit(1);
        }

        try {
            new URL(url);
        } catch (MalformedURLException e) {
            logger.error(String.format("mailformed image url: %s", url));
            System.exit(1);
        }
    }

    private static void checkImageResolutionArgs(String[] args) {

        int w = 0;
        int h = 0;
        String width = getArgValue("-w", args);
        if (width.isEmpty()) {
            logger.error("image width is not specified." + USAGE);
            System.exit(1);
        } else {
            try {
                w = Integer.parseInt(width);
            } catch (NumberFormatException e) {
                logger.error("image width should be an integer value");
                System.exit(1);
            }
        }

        String height = getArgValue("-h", args);
        if (height.isEmpty()) {
            logger.error("image height is not specified." + USAGE);
            System.exit(1);
        } else {
            try {
                h = Integer.parseInt(height);
            } catch (NumberFormatException e) {
                logger.error("image height should be an integer value");
                System.exit(1);
            }
        }

        if (w > 14008 || h > 9921) {
            logger.error("You cannot specify image pixel resolution greater than 14008x9921");
            System.exit(1);
        }

        if (w < 1 || h < 1) {
            logger.error("You cannot specify image pixel resolution less than 1x1");
            System.exit(1);
        }
    }

    private static File getDestFile(String[] args) {
        String fileName = Paths.get(getArgValue("-s", args)).getFileName().toString();
        return new File(getDestFolder(args), fileName);
    }

    private static File getDestFolder(String[] args) {

        String dest = getArgValue("-d", args);
        File destFolder;
        if (dest.isEmpty()) {
            return FileSystems.getDefault().getPath(".").toAbsolutePath().toFile();
        } else {
            destFolder = new File(dest);
            if (!destFolder.exists() && !destFolder.mkdirs()) {
                logger.error(String.format("unable to create dest folder %s", dest));
                System.exit(1);
            }
            return destFolder;
        }
    }

    private static boolean hasArg(String key, String[] args) {
        for (String arg : args) {
            if(arg.equalsIgnoreCase(key)) {
                return true;
            }
        }
        return false;
    }

    private static String getArgValue(String argKey, String[] args) {

        boolean found = false;
        for (String arg : args) {
            if (found) {
                return arg;
            } else if (arg.equalsIgnoreCase(argKey)) {
                found = true;
            }
        }
        return "";
    }

}
