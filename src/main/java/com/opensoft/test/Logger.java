package com.opensoft.test;

public interface Logger {

    void info(String msg);

    void error(String msg);
}
