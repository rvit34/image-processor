package com.opensoft.test;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class MultiThreadedGrayFilter extends GrayFilter {

    private final int cpuCount;
    private final ExecutorService executors;
    private final Logger logger;

    private volatile CountDownLatch startLatch;

    public MultiThreadedGrayFilter(int cpuCount, Logger logger) {
        this.cpuCount = cpuCount;
        this.executors = Executors.newWorkStealingPool(cpuCount);
        this.logger = logger;
    }

    @Override
    public BufferedImage process(BufferedImage image) {

        logger.info(String.format("%d CPUs will be utilized to do the task", cpuCount));

        final int imgWidth = image.getWidth();
        final int imgHeight = image.getHeight();

        final int blockImgWidth = imgWidth / cpuCount;
        final int blockImgHeight = imgHeight / cpuCount;

        final int blocksCount = cpuCount * cpuCount;
        List<GrayScaleTask> tasks = new ArrayList<>(blocksCount);

        int x, y;
        for (int i=0;i<cpuCount;i++) {
            x = i*blockImgWidth;
            for (int j=0;j<cpuCount;j++) {
                y = j*blockImgHeight;
                tasks.add(new GrayScaleTask(image, x, y,  x + blockImgWidth, y + blockImgHeight));
            }
        }

        final int subImgHeight = blockImgHeight * cpuCount;
        final int subImgWidth = blockImgWidth * cpuCount;

        // and let's add 3 additional tasks to complete processing image pixel matrix
        tasks.add(new GrayScaleTask(image, 0, subImgHeight, imgWidth - 1, imgHeight - subImgHeight));
        tasks.add(new GrayScaleTask(image, subImgWidth, 0, imgWidth - subImgWidth, imgHeight -1));
        tasks.add(new GrayScaleTask(image, subImgWidth, subImgHeight, imgWidth, imgHeight));

        startLatch = new CountDownLatch(tasks.size());
        try {
            executors.invokeAll(tasks);
        } catch (InterruptedException e) {
            logger.error(String.format("accidentally tasks were interrupted. Cause: %s", e.getMessage()));
        }

        final int timeout = 10;
        try {
            logger.info("awaiting tasks completion...");
            if (!startLatch.await(timeout, TimeUnit.SECONDS)) {
                logger.error(String.format("computation takes too long (>%d seconds). So we exit", timeout));
            } else {
                logger.info("done");
            }
        } catch (InterruptedException e) {
            logger.error(String.format("computation takes too long (>%d seconds). So we exit", timeout));
        }

        return image;
    }

    private class GrayScaleTask implements Callable<Boolean> {

        private final BufferedImage image;
        private final int i, j, width, height;

        private GrayScaleTask(BufferedImage image, int i, int j, int width, int height) {
            this.image = image;
            this.i = i;
            this.j = j;
            this.width = width;
            this.height = height;
        }

        @Override
        public Boolean call() {

            for (int x = i; x < width; ++x) {
                for (int y = j; y < height; ++y) {
                    image.setRGB(x, y, ((javax.swing.GrayFilter)imageFilter).filterRGB(x, y, image.getRGB(x, y)));
                }
            }

            startLatch.countDown();
            return true;
        }

    }
}
