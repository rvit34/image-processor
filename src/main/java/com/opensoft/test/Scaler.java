package com.opensoft.test;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Scaler implements ImageProcessor {

    private final int width;
    private final int height;

    Scaler(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public BufferedImage process(BufferedImage source) {
        return toBufferedImage(source.getScaledInstance(width, height, Image.SCALE_SMOOTH));
    }
}
