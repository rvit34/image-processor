package com.opensoft.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class GrayScaleTest {

    protected GrayScale grayScale;

    protected static final int RES_IMG_WIDTH = 100;
    protected static final int RES_IMG_HEIGHT = 100;

    static Logger logger = new ConsoleLogger();

    @Before
    public void setUp() {
        grayScale = new GrayScale(
                new GrayFilter(),
                new Scaler(RES_IMG_WIDTH, RES_IMG_HEIGHT)
        );
    }

    @Test
    public void testImageFormats() throws IOException {
        testImage("png");
        testImage("jpg");
        testImage("bmp");
    }

    protected void testImage(String imageFormat) throws IOException {
        assertThatImageIsGrayScale(grayScale.process(ImageIO.read(new File("./src/test/resources/images/test." + imageFormat))));
    }

    private static void assertThatImageIsGrayScale(BufferedImage image) {
        Assert.assertTrue(image.getHeight() == RES_IMG_HEIGHT);
        Assert.assertTrue(image.getWidth() == RES_IMG_WIDTH);
        Assert.assertTrue(isGrey(image));
    }

    private static boolean isGrey(BufferedImage image)
    {
        int pixel, red, green, blue;
        for (int i = 0; i < image.getWidth(); i++)
        {
            for (int j = 0; j < image.getHeight(); j++)
            {
                pixel = image.getRGB(i, j);
                red = (pixel >> 16) & 0xff;
                green = (pixel >> 8) & 0xff;
                blue = (pixel) & 0xff;
                if (red != green || green != blue ) {
                    logger.error(String.format("pixel[%d,%d] is not gray", i, j));
                    return false;
                }
            }
        }
        return true;
    }
}