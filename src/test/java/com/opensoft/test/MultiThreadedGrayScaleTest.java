package com.opensoft.test;

import org.junit.Before;

public class MultiThreadedGrayScaleTest extends GrayScaleTest {

    @Override
    @Before
    public void setUp() {
        grayScale = new GrayScale(
                new MultiThreadedGrayFilter(Runtime.getRuntime().availableProcessors(), logger),
                new Scaler(RES_IMG_WIDTH, RES_IMG_HEIGHT)
        );
    }
}
